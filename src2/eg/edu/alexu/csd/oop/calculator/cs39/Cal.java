package eg.edu.alexu.csd.oop.calculator.cs39;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

import eg.edu.alexu.csd.oop.calculator.Calculator;

public class Cal implements Calculator {
	double num1, num2;
	String str1="", str2="", str3="";
	double result=0;
	String formula="";
	String arr[]=new String[5];
	int arrIndex=0;
	
	public void input(String s){
		if(arrIndex==5){
			for(int j=0;j<4;j++){
				arr[j]=arr[j+1];
			}
			arr[4]=s;
		}
		else {
			arr[arrIndex]=s;
			arrIndex++;
		}
		formula=s;
		int i;
		for(i=0;i<s.length() && !isOperator(s.charAt(i));i++){
				str1=str1+s.charAt(i);
		}
		str3=str3+s.charAt(i);
		i++;
		while(i<s.length()){
			str2=str2+s.charAt(i);
			i++;
		}
		num1=Double.parseDouble(str1);
		num2=Double.parseDouble(str2);
	}
	public String getResult(){
		if(str3=="+"){
			result=num1+num2;
		}
		if(str3=="-"){
			result=num1-num2;
		}
		if(str3=="*"){
			result=num1*num2;
		}
		if(str3=="/"){
			result=num1/num2;
		}
		else{
			throw null;
		}
		return String.valueOf(result);
	}
	public String current(){
		return formula;
	}
	public String prev(){
		if (arrIndex<2){
			return null;
		}
		return arr[arrIndex-2];
	}
	public String next(){
		if(arrIndex>3){
			throw null;
		}
		if(arr[arrIndex]==null){
			throw null;
		}
		return arr[arrIndex];
	}
	public void save(){
		try
		{
		    PrintWriter pr = new PrintWriter("file");    

		    for (int i=0; i<arr.length ; i++)
		    {
		        pr.println(arr[i]);
		    }
		    pr.close();
		}
		catch (Exception e)
		{
		    e.printStackTrace();
		    System.out.println("No such file exists.");
		}
	}
	public void load(){
		int i=0;
		Scanner scan;
		try {
			scan = new Scanner(new File("/src2/eg/edu/alexu/csd/oop/calculator/cs39/file"));
			while (scan.hasNextLine()) {
				  arr[i]=scan.nextLine();
				}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public boolean isOperator(char a){
		if(a=='/' ||a=='*' ||a=='-' ||a=='+'){
			return true;
		}
		return false;
	}
}
