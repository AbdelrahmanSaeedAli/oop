package eg.edu.alexu.csd.oop.calculator.cs39;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;

import eg.edu.alexu.csd.oop.calculator.Calculator;

public class Cal implements Calculator  {
	double num1, num2;
	String str1, str2;
	char str3;
	double result  = 0;
	String formula = "";
	String arr[] = new String[5];
	int topIndex = 0, currIndex = 0;
	
	public void input(String s) {
		if (topIndex == 5) {
			for (int j = 0;j < 4;j++) {
				arr[j] = arr[j+1];
			}
			arr[4] = s;
			currIndex=4;
		}
		else  {
			arr[topIndex] = s;
			currIndex = topIndex;
			topIndex++;
		}
		formula = s;
		cut(formula);
	}
	public String getResult() {
		if (str3 == '+') {
			result = num1+num2;
		}
		else if (str3 == '-') {
			result = num1-num2;
		}
		else if (str3 == '*') {
			result = num1*num2;
		}
		else if (str3 == '/') {
			result = num1/num2;
		}
		
		return String.valueOf(result);
	}
	public String current() {
		if (formula == "") {
			return null;
		}
		return formula;
	}
	public String prev() {
		if (currIndex == 0) {
			return null;
		}
		currIndex = currIndex-1;
		formula = arr[currIndex];
		cut(formula);
		return arr[currIndex];
	}
	public String next() {
		if (currIndex == 4 || currIndex == topIndex-1 || topIndex == 0) {
			return null;
		}
		currIndex++;
		formula = arr[currIndex];
		cut(formula);
		return arr[currIndex];
	}
	
	public void save() {
		try {
    		File file = new File("file.txt");
    		FileWriter writer = new FileWriter(file);
    		writer.write(formula + '@' + currIndex + topIndex + System.getProperty("line.separator"));
    		for (int i = 0; i<arr.length && arr[i]!=""; i++)
    		{
    			writer.write(arr[i]+System.getProperty("line.separator"));            
    		}	
		    writer.close();
		}
		catch (Exception e)
		 {
		    e.printStackTrace();
		    System.out.println("No such file exists.");
		}
	}
	public void load() {
		int i = 0;
		int l;
		Scanner scan;
		try  {
			scan  =  new Scanner(new File("file.txt"));
			clearArr();
			currIndex = 0;
			formula = "";
			if (scan.hasNextLine()) {
				String temp;
				temp = scan.nextLine();
				for (l = 0;l < temp.length() && temp.charAt(l) != '@';l++) {
					formula = formula + temp.charAt(l);
				}
				if (formula != "") {
					cut(formula);
				}
				else {
					scan.close();
					return ;
				}
				currIndex = Character.getNumericValue(temp.charAt(l+1));
				topIndex = Character.getNumericValue(temp.charAt(l+2));
				while (scan.hasNextLine())  {
					arr[i] = scan.nextLine();
					i++;
				}
			}
			scan.close();
		} catch (FileNotFoundException e)  {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw null;
		}
	}
	public void clearArr() {
		for (int i = 0;i<5;i++) {
			arr[i] = "";
		}
	}
	public void cut(String s) {
		num1 = 0;
		num2 = 0 ;
		str1 = "";
		str2 = "";
		int i;
		for (i = 0;i < s.length() && !isOperator(s.charAt(i));i++) {
				str1 = str1+s.charAt(i);
		}
		str3 = s.charAt(i);
		i++;
		while (i<s.length()) {
			str2 = str2+s.charAt(i);
			i++;
		}
		num1 = Double.parseDouble(str1);
		num2 = Double.parseDouble(str2);
	}
	public boolean isOperator(char a) {
		if (a == '/' ||a == '*' ||a == '-' ||a == '+') {
			return true;
		}
		return false;
	}
}
