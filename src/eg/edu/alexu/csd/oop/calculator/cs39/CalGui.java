package eg.edu.alexu.csd.oop.calculator.cs39;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.Font;

public class CalGui {
	private String word="";
	private JFrame frame;
	Cal object = new Cal();
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CalGui window = new CalGui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CalGui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 694, 433);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton button = new JButton("7");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				word=word+"7";
			}
		});
		button.setBounds(12, 155, 72, 25);
		frame.getContentPane().add(button);
		
		JButton button_1 = new JButton("4");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				word=word+"4";
			}
		});
		button_1.setBounds(12, 193, 72, 25);
		frame.getContentPane().add(button_1);
		
		JButton btnNewButton = new JButton("1");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				word=word+"1";
			}
		});
		btnNewButton.setBounds(12, 231, 72, 25);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_2 = new JButton("2");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				word=word+"2";
			}
		});
		btnNewButton_2.setBounds(106, 231, 72, 25);
		frame.getContentPane().add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("5");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				word=word+"5";
			}
		});
		btnNewButton_3.setBounds(106, 193, 72, 25);
		frame.getContentPane().add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("8");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				word=word+"8";
			}
		});
		btnNewButton_4.setBounds(106, 155, 72, 25);
		frame.getContentPane().add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("3");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				word=word+"3";
			}
		});
		btnNewButton_5.setBounds(200, 231, 72, 25);
		frame.getContentPane().add(btnNewButton_5);
		
		JButton btnNewButton_6 = new JButton("6");
		btnNewButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				word=word+"6";
			}
		});
		btnNewButton_6.setBounds(200, 193, 72, 25);
		frame.getContentPane().add(btnNewButton_6);
		
		JButton btnNewButton_7 = new JButton("9");
		btnNewButton_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				word=word+"9";
			}
		});
		btnNewButton_7.setBounds(200, 155, 72, 25);
		frame.getContentPane().add(btnNewButton_7);
		
		JButton button_2 = new JButton("0");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				word=word+"0";
			}
		});
		button_2.setBounds(12, 269, 72, 25);
		frame.getContentPane().add(button_2);
		
		JButton btnNewButton_1 = new JButton("enter");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				object.input(word);
			}
		});
		btnNewButton_1.setBounds(567, 257, 97, 116);
		frame.getContentPane().add(btnNewButton_1);
		
		JButton btnGetResult = new JButton("get result");
		btnGetResult.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				object.getResult();
				textField.setText(object.getResult());
			}
		});
		btnGetResult.setBounds(449, 257, 97, 116);
		frame.getContentPane().add(btnGetResult);
		
		JButton btnNext = new JButton("next");
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				object.next();
			}
		});
		btnNext.setBounds(449, 219, 97, 25);
		frame.getContentPane().add(btnNext);
		
		JButton btnPrevious = new JButton("previous");
		btnPrevious.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				object.prev();
			}
		});
		btnPrevious.setBounds(567, 219, 97, 25);
		frame.getContentPane().add(btnPrevious);
		
		JButton btnSave = new JButton("save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				object.save();
			}
		});
		btnSave.setBounds(449, 181, 97, 25);
		frame.getContentPane().add(btnSave);
		
		JButton btnLoad = new JButton("load");
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				object.load();
			}
		});
		btnLoad.setBounds(567, 181, 97, 25);
		frame.getContentPane().add(btnLoad);
		
		JButton btnCurrent = new JButton("current");
		btnCurrent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				object.current();
			}
		});
		btnCurrent.setBounds(449, 143, 97, 25);
		frame.getContentPane().add(btnCurrent);
		
		
		JButton btnNewButton_8 = new JButton("+");
		btnNewButton_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				word=word+"+";
			}
		});
		btnNewButton_8.setBounds(276, 155, 41, 51);
		frame.getContentPane().add(btnNewButton_8);
		
		JButton button_3 = new JButton("-");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				word=word+"-";
			}
		});
		button_3.setBounds(276, 205, 41, 51);
		frame.getContentPane().add(button_3);
		
		JButton button_4 = new JButton("*");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				word=word+"*";
			}
		});
		button_4.setBounds(321, 155, 41, 51);
		frame.getContentPane().add(button_4);
		
		JButton button_5 = new JButton("/");
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				word=word+"/";
			}
		});
		button_5.setBounds(321, 205, 41, 51);
		frame.getContentPane().add(button_5);
		
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 36));
		textField.setBounds(81, 30, 508, 71);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
	}
}
