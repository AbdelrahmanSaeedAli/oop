package eg.edu.alexu.csd.oop.draw39;

import eg.edu.alexu.csd.oop.draw.Shape;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.HashMap;
import java.util.Map;

public class Rectangle implements Shape{
	
	protected Point p;
    protected Map<String, Double> prop;
    protected Color c;
    protected Color fc;
    
    public Rectangle() {
        prop = new HashMap<>();
        prop.put("Width", 0.0);
        prop.put("Length", 0.0) ;
        
    }
    
	public void setPosition(java.awt.Point position){
    	p = position ;
	
}
	
	public java.awt.Point getPosition(){
		
		return p ;
		
	}
	
	/* update shape specific properties (e.g., radius) */
	public void setProperties(java.util.Map<String, Double> properties){
		prop = properties ;
		
	}
	
	public java.util.Map<String, Double> getProperties(){
		return prop ;
	}
	
	public void setColor(java.awt.Color color){
		c = color ;
		
	}
	
	public void setFillColor(java.awt.Color color){
		fc = color ;
		
	}
	public java.awt.Color getColor(){
		return c ;
		
	}
	
	
	
	public java.awt.Color getFillColor(){
		return fc ;
	}
	
	/* redraw the shape on the canvas */
	public void draw(java.awt.Graphics canvas){
		((Graphics2D) canvas).setColor(getFillColor());
        ((Graphics2D) canvas).fillRect((int) p.getX(),
                (int) p.getY(),
                (int) prop.get("Width").intValue(),
                (int) prop.get("Length").intValue());

        ((Graphics2D) canvas).setStroke(new BasicStroke(2));
        ((Graphics2D) canvas).setColor(getColor());
        ((Graphics2D) canvas).drawRect((int) p.getX(),
                (int) p.getY(),
                (int) prop.get("Width").intValue(),
                (int) prop.get("Length").intValue());
		
	}
	
	/* create a deep clone of the shape */
	public Object clone() throws CloneNotSupportedException{
		
		Shape r = new Rectangle();
        r.setColor(c);
        r.setFillColor(fc);
        r.setPosition(p);
        Map newprop = new HashMap<>();
        for (Map.Entry s: prop.entrySet())
            newprop.put(s.getKey(), s.getValue());
        r.setProperties(newprop);
        return r;
		
	}

}
