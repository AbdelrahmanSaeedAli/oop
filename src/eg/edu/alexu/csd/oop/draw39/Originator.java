package eg.edu.alexu.csd.oop.draw39;

import eg.edu.alexu.csd.oop.draw.Shape;

public class Originator{
	
	private TwoShapes shape;
	public Originator(){
		this.shape = new TwoShapes() ;
	}

	// Sets the value for the Shape
	
	public void set(Shape oldShape,Shape newShape) { 
		//System.out.println("From Originator: Current Version of Article\n"+newArticle+ "\n");
		this.shape.setOldShape(oldShape);
		this.shape.setNewShape(newShape);
	}

	// Creates a new Memento with a new Shape
	
	public Memento storeInMemento() { 
	    System.out.println("From Originator: Saving to Memento");
	    return new Memento(shape.getOldShape() ,shape.getNewShape()); 
	}
	   
	// Gets the Shape currently stored in memento
	
	public TwoShapes restoreFromMemento(Memento memento) {
		  
		shape.setOldShape(memento.getOldShape());
		shape.setNewShape(memento.getNewShape());
		//System.out.println("From Originator: Previous Article Saved in Memento\n"+article + "\n");
		
		return shape;
	   
	}
	
}