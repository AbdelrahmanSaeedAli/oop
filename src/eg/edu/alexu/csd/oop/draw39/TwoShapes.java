package eg.edu.alexu.csd.oop.draw39;

import eg.edu.alexu.csd.oop.draw.Shape;

public class TwoShapes {
	private Shape oldShape ;
	private Shape newShape ;
	
	public void setNewShape(Shape newShape) {
		this.newShape = newShape;
	}

	public Shape getOldShape() {
		return oldShape;
	}

	public void setOldShape(Shape oldShape) {
		this.oldShape = oldShape;
	}
	public Shape getNewShape() {
		return newShape;
	}
	

}
