package eg.edu.alexu.csd.oop.draw39;
import eg.edu.alexu.csd.oop.draw.Shape;

public class Memento {
	
	// The article stored in memento Object
	
	private TwoShapes shape;


	// Save a new article String to the memento Object
	
	public Memento(Shape oldShape ,Shape newShape) {
		this.shape = new TwoShapes() ;
		this.shape.setOldShape(oldShape);
		this.shape.setNewShape(newShape);
		}
	
	// Return the value stored in article 
	
	public Shape getOldShape() {
		return shape.getOldShape(); 
		}
	public Shape getNewShape() {
		return shape.getNewShape(); 
		}
}
