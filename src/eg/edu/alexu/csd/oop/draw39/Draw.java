package eg.edu.alexu.csd.oop.draw39;

import java.awt.Color;
import java.awt.Point;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.xml.sax.*;

import org.w3c.dom.*;

import eg.edu.alexu.csd.oop.draw.DrawingEngine;
import eg.edu.alexu.csd.oop.draw.Shape;



public class Draw implements DrawingEngine {
	private LinkedList<Shape> shapesList;
	Caretaker caretaker ;
	Originator originator ;
	int savedShapes , currentShape ;
	List<Class<? extends Shape>> supprotedShapes ;
	
	public Draw(){
		this.supprotedShapes = new ArrayList<>();
		this.shapesList = new LinkedList<Shape>() ;
		this.caretaker = new Caretaker();
		this.originator = new Originator();
		this.savedShapes = 0 ;
		this.currentShape = 0 ;
		this.supprotedShapes.add(Circle.class) ;
		this.supprotedShapes.add(Rectangle.class) ;
		this.supprotedShapes.add(Square.class) ;
		this.supprotedShapes.add(Line.class) ;
		this.supprotedShapes.add(Triangle.class) ;
		this.supprotedShapes.add(Ellipse.class) ;
	}


	/* redraw all shapes on the canvas */
	public void refresh(java.awt.Graphics canvas){
		for (int i = 0 ; i < this.shapesList.size() ; i ++)
		{
			this.shapesList.get(i).draw(canvas);
		}
		
	}
	public void addShape(Shape shape){
		for (int i = caretaker.savedShapes.size() - 1 ; i > caretaker.currentShape - 1 ; i--) {
			caretaker.savedShapes.remove(i);
		}
		this.shapesList.add(shape) ;
		originator.set(null, shape);
		caretaker.addMemento(originator.storeInMemento());
		
		currentShape = caretaker.currentShape ;
		System.out.println(this.shapesList.size());
		System.out.println(this.caretaker.currentShape);
	}
	public void removeShape(Shape shape){
		for (int i = caretaker.savedShapes.size() - 1; i > caretaker.currentShape; i--) {
			caretaker.savedShapes.remove(i);
		}
		if (this.shapesList.contains(shape))
		{
			this.shapesList.remove(shape) ;
			
			originator.set(shape, null);
			caretaker.addMemento(originator.storeInMemento());
			
			currentShape = caretaker.currentShape ;
			System.out.println(this.shapesList.size());
			System.out.println(this.caretaker.currentShape);
		}
		
	}
	public void updateShape(Shape oldShape, Shape newShape){
		for (int i = caretaker.savedShapes.size() - 1; i > caretaker.currentShape; i--) {
			caretaker.savedShapes.remove(i);
		}
		this.shapesList.remove(oldShape);
		this.shapesList.add(newShape) ;
		
		originator.set(oldShape, newShape);
		caretaker.addMemento(originator.storeInMemento());
		
		currentShape = caretaker.currentShape ;
		System.out.println(this.shapesList.size());
		System.out.println(this.caretaker.currentShape);
		
	}
	/* return the created shapes objects */
	public Shape[] getShapes(){
		Shape[] shapes = new Shape[this.shapesList.size()] ;
		for (int i = 0 ; i < shapes.length ; i ++)
		{
			shapes[i] = shapesList.get(i) ;
		}
		return shapes ;
		
	}
	/* return the classes (types) of supported shapes that can
	* be dynamically loaded at runtime (see Part 3) */
	public java.util.List<Class<? extends Shape>> getSupportedShapes(){
		
		return this.supprotedShapes ;
		
	}
	/* limited to 20 steps. You consider these actions in
	* undo & redo: addShape, removeShape, updateShape */
	public void undo(){
		
		if (caretaker.currentShape > 0)
		{
			this.caretaker.currentShape -- ;
			TwoShapes memento = originator.restoreFromMemento( caretaker.getMemento(caretaker.currentShape));
			if (memento.getOldShape() == null)
			{
				this.shapesList.remove(memento.getNewShape()) ;
			}
			else if (memento.getNewShape() == null)
			{
				this.shapesList.add(memento.getOldShape()) ;
			}
			else
			{
				this.shapesList.remove(memento.getNewShape());
				this.shapesList.add(memento.getOldShape()) ;
			}
		}
		System.out.println(this.shapesList.size());
		System.out.println(this.caretaker.currentShape);
		
	}
	public void redo(){

		
		if (caretaker.currentShape < caretaker.savedShapes.size()  )
		{
			TwoShapes memento = originator.restoreFromMemento( caretaker.getMemento(caretaker.currentShape));
			if (memento.getOldShape() == null)
			{
				this.shapesList.add(memento.getNewShape()) ;
			}
			else if (memento.getNewShape() == null)
			{
				this.shapesList.remove(memento.getOldShape()) ;
			}
			else
			{
				this.shapesList.remove(memento.getOldShape()) ;
				this.shapesList.add(memento.getNewShape());
			}
			this.caretaker.currentShape ++ ;
		}
		System.out.println(this.shapesList.size());
		System.out.println(this.caretaker.currentShape);
	}
	/* use the file extension to determine the type,
	* or throw runtime exception when unexpected extension */
	public void save(String path){
			saveXML(path , this.shapesList) ;
		
	}
	public void load(String path){
			loadFromXML(path ) ;
		
	}
	public void saveXML(String path , LinkedList<Shape> shapes){
		try 
		{
			Formatter w = new Formatter("File.xml");
				
		}
		catch (Exception e )
		{
			
		}
		Document dom = null ;
		Element shape = null ;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder db = dbf.newDocumentBuilder() ;
			dom = db.newDocument() ;
			Element rootElement = dom.createElement("ShapesList") ;
			rootElement.setAttribute("size", this.shapesList.size()+"");
			for (int i = 0 ; i < this.shapesList.size() ; i ++)
			{
				try {
					shape = dom.createElement("Shape" + i) ;
					shape.setAttribute("name", this.shapesList.get(i).getClass().getName());
					shape.setAttribute("color", this.shapesList.get(i).getColor().getRGB()+"");
					shape.setAttribute("fillColor", this.shapesList.get(i).getFillColor().getRGB()+"");
					shape.setAttribute("positionX", this.shapesList.get(i).getPosition().getX()+"");
					shape.setAttribute("positionY", this.shapesList.get(i).getPosition().getY()+"");
					Element prop = dom.createElement("prop") ;
					for (String key : this.shapesList.get(i).getProperties().keySet() )
					{
						prop.setAttribute(key, this.shapesList.get(i).getProperties().get(key)+ "" );
					}
					
					shape.appendChild(prop) ;
					
				}catch (Exception e)
				{
					shape = dom.createElement("Shape" + i) ;
					shape.setAttribute("name", this.shapesList.get(i).getClass().getName());
					shape.setAttribute("color","0");
					shape.setAttribute("fillColor", "0");
					shape.setAttribute("positionX", "0");
					shape.setAttribute("positionY", "0");
					Element prop = dom.createElement("prop") ;
					
					
					shape.appendChild(prop) ;
				}
				rootElement.appendChild(shape);
				
			}
			dom.appendChild(rootElement);
		}
		catch (Exception e )
		{
			
		}
		try{
			Transformer tr = TransformerFactory.newInstance().newTransformer();
			tr.setOutputProperty(OutputKeys.INDENT, "yes");
			tr.setOutputProperty(OutputKeys.METHOD, "xml");
			tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			tr.transform(new DOMSource(dom),new StreamResult(new FileOutputStream(path)));
		}catch (TransformerException te) {
			System.out.println(te.getMessage());
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
		}
		
	} 
	public void loadFromXML(String path ) {
		
		this.shapesList.clear();
		/*File file = new File(path);
		InputStream inputStream;
		Reader reader = null;
		try {
			inputStream = new FileInputStream(file);
			try {
				reader = new InputStreamReader(inputStream, "ISO-8859-1");
			} catch (UnsupportedEncodingException e) {
			
				e.printStackTrace();
			}
		} catch (FileNotFoundException e1) {
			
			e1.printStackTrace();
		}
		InputSource is = new InputSource(reader);
		is.setEncoding("ISO-8859-1");
		*/
		Document dom;
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = builderFactory.newDocumentBuilder();
			dom = builder.parse(path);
			Element shapeList = dom.getDocumentElement() ;
			int size = Integer.parseInt(shapeList.getAttribute("size")) ;
			for (int i = 0 ; i < size ; i ++)
			{
				Element shape = (Element)shapeList.getElementsByTagName("Shape" + i ).item(0) ;
				Shape newShape = (Shape)Class.forName(
						shape.
						getAttribute("name"))
						.getConstructor().newInstance() ;
				newShape.setColor(new Color(Integer.parseInt(shape.getAttribute("color"))));
				newShape.setFillColor(new Color(Integer.parseInt(shape.getAttribute("fillColor"))));
				 Point point = new Point() ;
				 point.x = (int) Double.parseDouble(shape.getAttribute("positionX")) ;
				 point.y = (int) Double.parseDouble(shape.getAttribute("positionY")) ;
				newShape.setPosition(point);
				Element prop = (Element) shape.getElementsByTagName("prop").item(0) ;
				Map<String,Double> shapeProp = new HashMap<>() ;
				NamedNodeMap attributes = prop.getAttributes() ;
				for (int j = 0 ; j < attributes.getLength() ; j ++)
				{
					shapeProp.put(attributes.item(j).getNodeName(), Double.parseDouble(attributes.item(j).getNodeValue()));
				}
				newShape.setProperties(shapeProp);
				this.shapesList.add(newShape);
				
			}
		}catch (Exception e )
		{
			e.printStackTrace();
		}
		this.caretaker.savedShapes.clear();
		
		this.caretaker.currentShape = 0 ;
	}
	
	

}
